# usage: python extract_lexicon.py ../ccsr-files/experiment/gold/wsj02-21.stagged ../ccsr-files/experiment/gold/wsj00.stagged ../ccsr-files/experiment/gold/wsj23.stagged ../ccsr-files/super/lexicon ../ccsr-files/super/classes

import sys
import cPickle as pickle
from collections import defaultdict

def gen_dict(f, d, ld, train_set):
  f.seek(0)
  total_skipped = 0
  total_sent = 0
  total_tokens = 0
  for l in f.readlines():
    if l.startswith('# this file') or l.startswith('# src/scripts'): 
      continue
    if not l.strip():
      continue
    total_sent += 1
    tokens = l.rstrip('\n').split()
    
    if train_set: 
      skip = False 
      for t in tokens:
        if not t.split('|')[2] in ld:
          skip = True
          total_skipped += 1
          break
      if skip: continue

    total_tokens += len(tokens)
    for t in tokens:
      w = t.split('|')[0]
      if w in d:
        continue
      else:
        d[w] += 1
  sys.stderr.write('total sent: ' + str(total_sent) + '\n')
  sys.stderr.write('total skipped: ' + str(total_skipped) + '\n')
  sys.stderr.write('total tokens: ' + str(total_tokens) + '\n')

# add the missing ones in CCGBank1.2 from super lexicon
#def gen_dict2(d):
#  l_count = 0
#  for l in open(sys.argv[4]).readlines():
#    l_count += 1
#    if l_count < 5: continue
#    if d[l.split()[0]] == 0:
#      d[l.split()[0]] += 1

def gen_classes(d):
  l_count = 0
  for l in open(sys.argv[1]).readlines():
    l_count += 1
    if l_count < 4: continue
    for t in l.split():
      d[t.split('|')[2]] += 1

# for mapping train set 
def map_stag(f, wd, ld):
  total_skipped = 0
  f.seek(0)
  words_idx = []
  labels_idx = []
  i = 0 
  for l in f.readlines():
    i += 1
    if l.startswith('# this file') or l.startswith('# src/scripts'): 
      continue
    if not l.strip():
      continue
    tokens = l.rstrip('\n').split()
    
    skip = False
    for t in tokens:
      if not t.split('|')[2] in ld:
        skip = True
        total_skipped += 1
        break
    if skip: continue
    #print map(lambda x: wd[x.split('|')[0]], tokens)
    words_idx.append(map(lambda x: wd[x.split('|')[0]], tokens))
    labels_idx.append(map(lambda x: ld[x.split('|')[2]], tokens)) 
    assert(len(map(lambda x: wd[x.split('|')[0]], tokens))==len(tokens))
    assert(len(map(lambda x: ld[x.split('|')[2]], tokens))==len(tokens))
    
  print "total skipped: " + str(total_skipped)
  assert(len(words_idx)==len(labels_idx))
  return (words_idx, labels_idx)

# for mapping dev and test sets
def map_stag2(f, wd, ld):
  f.seek(0)
  words_idx = []
  labels_idx = []
  for l in f.readlines():
    if l.startswith('# this file') or l.startswith('# src/scripts'):
      continue
    if not l.strip():
      continue
    tokens = l.rstrip('\n').split()
    idx = map(lambda x: wd[x.split('|')[0]], tokens)
    words_idx.append(idx) 
   
    tmp = []
    for t in tokens:
      cat = t.split('|')[2] 
      if cat in ld:
        tmp.append(ld[cat])
      else: 
        tmp.append(ld['NNOONNEE'])
    assert(len(idx)==len(tmp))
    labels_idx.append(tmp)
  assert(len(words_idx) == len(labels_idx))
  return (words_idx, labels_idx)
      
if __name__ == '__main__':
  f_train = open(sys.argv[1])
  f_dev = open(sys.argv[2])
  f_test = open(sys.argv[3])
  d = defaultdict(int)
  d2 = defaultdict(int)
  gen_classes(d2)

  label2idx_dict = {}
  for k,v in d2.iteritems():
    label2idx_dict[k] = len(label2idx_dict) 
  label2idx_dict['NNOONNEE'] = len(label2idx_dict)
  sys.stderr.write('total classes no skipping: ' + str(len(label2idx_dict)) + '\n')

  gen_dict(f_train, d, d2, False) 
  gen_dict(f_dev, d, d2, False)
  gen_dict(f_test, d, d2, False)
  
  word2idx_dict = {}
  for k,v in d.iteritems():
    word2idx_dict[k] = len(word2idx_dict) 

  sys.stderr.write('total voc size: ' + str(len(word2idx_dict)) + '\n')
  
  train_data = map_stag2(f_train, word2idx_dict, label2idx_dict)
  print 'train set size: ' + str(len(train_data[1]))
  dev_data = map_stag2(f_dev, word2idx_dict, label2idx_dict)
  test_data = map_stag2(f_test, word2idx_dict, label2idx_dict) 
  
  sys.stderr.write('total classes with skipping: ' + \
    str(len(set([x for sublist in train_data[1] + dev_data[1] + test_data[1] for x in sublist]))) + '\n')
  dicts = {}
  dicts['words2idx'] = word2idx_dict
  dicts['labels2idx'] = label2idx_dict
  print 'labels2idx len: %d ' % len(label2idx_dict)
  print 'words2idx len: %d ' % len(word2idx_dict)
  data_all = (train_data, dev_data, test_data, dicts) 

  pickle.dump(data_all, open('ccg-super.pkl', 'wb'))
