import sys

def gen_tags(f):
  stags = []
  for l in open(f).readlines():
    tokens = l.rstrip('\n').split()
    stags.extend([x.split('|')[2] for x in tokens])
    return stags
   
if __name__ == '__main__':
  stags_gold = gen_tags(sys.argv[1])
  stags_result = gen_tags(sys.argv[2])
  correct = 0.0
  assert(len(stags_gold)==len(stags_result))
  for g, r in zip (stags_gold, stags_result):
    if g==r: correct += 1
  
  print 'accuracy: %2.2f' % (correct/float(len(stags_gold)))
  
